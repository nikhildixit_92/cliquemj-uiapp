//
//  Rewards_cell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 30/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class Rewards_cell: UITableViewCell {

    @IBOutlet weak var reward_custom_View: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.reward_custom_View.layer.cornerRadius = 3.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
