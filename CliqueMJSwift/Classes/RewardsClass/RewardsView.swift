//
//  RewardsView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 30/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class RewardsView: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    

    @IBOutlet weak var Refer_friend_btn: KButton!
    @IBOutlet weak var Hamburger_btn: UIButton!
    @IBOutlet weak var reward_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib.init(nibName: "Rewards_cell", bundle: nil)
        self.reward_table.register(nib, forCellReuseIdentifier: "Rewards_cell")
        
        self.reward_table.rowHeight = 135
        
        self.Hamburger_btn.addTarget(self, action: #selector(Hamburger_btn_Action), for: .touchUpInside)
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Rewards_cell", for: indexPath) as! Rewards_cell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    
    @IBAction func refer_btn_action(_ sender: Any)
    {
        let edit = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileView") as! EditProfileView
        self.present(edit, animated: true, completion: nil)
    }
    
    @objc func Hamburger_btn_Action()
    {
        self.dismiss(animated: true, completion: nil)
    }
}
