//
//  LeftDrawer.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class LeftDrawer: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    var Info_Arr = [""]
    @IBOutlet weak var left_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let nib = UINib.init(nibName: "left_profileCell", bundle: nil)
        self.left_table.register(nib, forCellReuseIdentifier: "left_profileCell")
        
        
        let nib1 = UINib.init(nibName: "locCell", bundle: nil)
        self.left_table.register(nib1, forCellReuseIdentifier: "locCell")
        
        let nib2 = UINib.init(nibName: "InfoCell", bundle: nil)
        self.left_table.register(nib2, forCellReuseIdentifier: "InfoCell")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.Info_Arr = ["Home","Collect Buds","Settings","My QR Code","Rewards","Invite Dispensary","Refer Friends"]
        self.left_table.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 2
        {
             return Info_Arr.count
        }
        else
        {
             return 1
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "left_profileCell", for: indexPath) as! left_profileCell
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "locCell", for: indexPath) as! locCell
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as! InfoCell
           
            cell.info_lbl.text = self.Info_Arr[indexPath.row]
            
            if indexPath.row == self.Info_Arr.count-1
            {
                 cell.info_lbl.textColor = UIColor.fromHexaString(hex: colorcodes().button_color)
            }
            else
            {
                cell.info_lbl.textColor = UIColor.fromHexaString(hex: colorcodes().text_color)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 81
        }
        else
        {
            return 44
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            UserDefaults.standard.set("editprofile", forKey: "drawer_id")
            
            if let drawerController = self.navigationController?.parent as? KYDrawerController
            {
                drawerController.setDrawerState(.closed, animated: true)
            }
        }
        else if indexPath.section == 1
        {
            UserDefaults.standard.set("Changelocation", forKey: "drawer_id")
            
            if let drawerController = self.navigationController?.parent as? KYDrawerController
            {
                drawerController.setDrawerState(.closed, animated: true)
            }
        }
       else if indexPath.section == 2
        {
            if indexPath.row == 0
            {
                UserDefaults.standard.set("home", forKey: "drawer_id")
                
                if let drawerController = self.navigationController?.parent as? KYDrawerController
                {
                    drawerController.setDrawerState(.closed, animated: true)
                }
            }
            else if indexPath.row == 1
            {
                UserDefaults.standard.set("collectbuds", forKey: "drawer_id")
                
                if let drawerController = self.navigationController?.parent as? KYDrawerController
                {
                    drawerController.setDrawerState(.closed, animated: true)
                }
            }
            else if indexPath.row == 2
            {
                UserDefaults.standard.set("settings", forKey: "drawer_id")
                
                if let drawerController = self.navigationController?.parent as? KYDrawerController
                {
                    drawerController.setDrawerState(.closed, animated: true)
                }
            }
            else if indexPath.row == 3
            {
                UserDefaults.standard.set("myqrcode", forKey: "drawer_id")
                
                if let drawerController = self.navigationController?.parent as? KYDrawerController
                {
                    drawerController.setDrawerState(.closed, animated: true)
                }
            }
            else if indexPath.row == 4
            {
                UserDefaults.standard.set("Rewards", forKey: "drawer_id")
                
                if let drawerController = self.navigationController?.parent as? KYDrawerController
                {
                    drawerController.setDrawerState(.closed, animated: true)
                }
            }
            else if indexPath.row == 5
            {
                UserDefaults.standard.set("inviteDispansary", forKey: "drawer_id")
                
                if let drawerController = self.navigationController?.parent as? KYDrawerController
                {
                    drawerController.setDrawerState(.closed, animated: true)
                }
            }
            else if indexPath.row == 6
            {
                UserDefaults.standard.set("referfriend", forKey: "drawer_id")
                
                if let drawerController = self.navigationController?.parent as? KYDrawerController
                {
                    drawerController.setDrawerState(.closed, animated: true)
                }
            }
        }
        else
        {
            
        }
    }
    
    @IBAction func policy_btn_action(_ sender: Any)
    {
        UserDefaults.standard.set("home", forKey: "drawer_id")
        
        if let drawerController = self.navigationController?.parent as? KYDrawerController
        {
            drawerController.setDrawerState(.closed, animated: true)
        }
    }
    @IBAction func terms_condition_action(_ sender: Any)
    {
        
    }
    @IBAction func logout_btn_action(_ sender: Any)
    {
        
    }

}
