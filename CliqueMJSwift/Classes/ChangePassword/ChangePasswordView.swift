//
//  ChangePasswordView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 30/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class ChangePasswordView: UIViewController {

    @IBOutlet weak var save_btn: UIButton!
    @IBOutlet weak var border_customView: UIView!
    @IBOutlet weak var user_pick_btn: UIButton!
    @IBOutlet weak var user_img: UIImageView!
    @IBOutlet weak var user_view: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.border_customView.clipsToBounds = true
        self.border_customView.layer.cornerRadius = 40
        self.border_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.user_img.layer.cornerRadius = self.user_img.frame.size.width/2
        self.user_img.layer.masksToBounds = true
    
        
        // Do any additional setup after loading the view.
    }

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true
            , completion: nil)
    }
    
    @objc func Save_btn_Action(sender:UIButton)
    {
       
    }
    @IBAction func save_action(_ sender: Any)
    {
        let redeem_noti = self.storyboard?.instantiateViewController(withIdentifier: "notifreceivedbudsView") as! notifreceivedbudsView
        self.present(redeem_noti, animated: true, completion: nil)
    }
}
