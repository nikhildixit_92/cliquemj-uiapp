//
//  LoginView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class LoginView: UIViewController {

    @IBOutlet weak var password_img: UIImageView!
    @IBOutlet weak var user_phone_img: UIImageView!
    @IBOutlet weak var Apple_ID_btn: UIButton!
    @IBOutlet weak var email_txt: CustomUITextField!
    @IBOutlet weak var donthave_lbl: UILabel!
    @IBOutlet weak var password_txt: CustomUITextField!
    @IBOutlet weak var show_password_btn: UIButton!
    @IBOutlet weak var forgot_password_btn: UIButton!
    @IBOutlet weak var sign_in_btn: KButton!
    @IBOutlet weak var fb_btn: UIButton!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        let complete_s = "Don’t have an account? Sign Up"
        let change_font = "Sign Up"
        
        let range = (complete_s as NSString).range(of: change_font)
        
        let attribute = NSMutableAttributedString.init(string: complete_s)
        attribute.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Inter-Bold", size: 14) , range: range)
        attribute.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue , range: range)
        self.donthave_lbl.attributedText = attribute
        
        self.user_phone_img.layer.cornerRadius = 5.0
        self.user_phone_img.layer.masksToBounds = true
        
        self.password_img.layer.cornerRadius = 5.0
        self.password_img.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func show_password_btn_Action(_ sender: Any)
    {
        
    }
    @IBAction func forgot_password_btn_Action(_ sender: Any)
    {
        let forgot = self.storyboard?.instantiateViewController(withIdentifier: "ForgotView") as! ForgotView
        self.present(forgot, animated: true, completion: nil)
    }
    
    @IBAction func sign_in_btn_action(_ sender: Any)
    {
        let main = UIStoryboard.init(name: "Main", bundle: nil)
        let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
        let navigation = UINavigationController.init(rootViewController: drawerC)
        navigation.setNavigationBarHidden(true, animated: true)
        appDelegate.window?.rootViewController = navigation
        appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func fb_btn_Action(_ sender: Any)
    {
       
    }
    
    @IBAction func dont_have_btn_Action(_ sender: Any)
    {
        let signup = self.storyboard?.instantiateViewController(withIdentifier: "SignUpView") as! SignUpView
        self.present(signup, animated: true, completion: nil)
     }
    @IBAction func Apple_ID_action(_ sender: Any)
    {
        
    }
}
