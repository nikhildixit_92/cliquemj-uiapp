//
//  NotificationDeliveryView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 29/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class NotificationDeliveryView: UIViewController {

    @IBOutlet weak var track_heading_txt: UILabel!
    @IBOutlet weak var track_delivery_sub_heading: UILabel!
    @IBOutlet weak var Track_delivery_btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func track_delivery_action(_ sender: Any)
    {
        let sucess = self.storyboard?.instantiateViewController(withIdentifier: "NotificationSuccessDeliveryView") as! NotificationSuccessDeliveryView
        self.present(sucess, animated: true, completion: nil)
    }
    
    @IBAction func dismiss_Action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
