//
//  NotificationTrackView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/06/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import MapKit

class NotificationTrackView: UIViewController,MKMapViewDelegate,UITableViewDelegate,UITableViewDataSource
{
    

    @IBOutlet weak var search_table_view: UITableView!
    @IBOutlet weak var map_view: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.map_view.delegate = self

        let nib = UINib.init(nibName: "Dispensary_trackFirstCell", bundle: nil)
        self.search_table_view.register(nib, forCellReuseIdentifier: "Dispensary_trackFirstCell")
        
        let nib1 = UINib.init(nibName: "NotificationDeliveryDetailsCell", bundle: nil)
        self.search_table_view.register(nib1, forCellReuseIdentifier: "NotificationDeliveryDetailsCell")
        
        let nib2 = UINib.init(nibName: "Notification_deliveryCell", bundle: nil)
        self.search_table_view.register(nib2, forCellReuseIdentifier: "Notification_deliveryCell")
        
        let nib3 = UINib.init(nibName: "Notification_deliveryCell", bundle: nil)
        self.search_table_view.register(nib3, forCellReuseIdentifier: "Notification_deliveryCell")
        
        let nib4 = UINib.init(nibName: "TrackingHistoryCell", bundle: nil)
        self.search_table_view.register(nib4, forCellReuseIdentifier: "TrackingHistoryCell")
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 2
        }
        else if section == 2
        {
            return 2
        }
        else
        {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Dispensary_trackFirstCell", for: indexPath) as! Dispensary_trackFirstCell
            cell.main_customView.clipsToBounds = true
            cell.main_customView.layer.cornerRadius = 30
            cell.main_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            cell.main_customView.isUserInteractionEnabled = true
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationDeliveryDetailsCell", for: indexPath) as! NotificationDeliveryDetailsCell
            
            if indexPath.row == 0
            {
                cell.order_txt.text = "Order Reference Number"
                cell.order_number_txt.text = "#12SD8AS7DGHAS"
            }
            else
            {
                cell.order_txt.text = "Tracking Number"
                cell.order_number_txt.text = "#8AS7DGHAS"
            }
            
            cell.backgroundColor = UIColor.fromHexaString(hex: colorcodes().Port_Gore)
            
            return cell
        }
        else if indexPath.section == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Notification_deliveryCell", for: indexPath) as! Notification_deliveryCell
            
            cell.heading_txt.text  = "Tracking History"
            cell.heading_txt.font = UIFont.init(name: "Inter-Regular", size: 12.0)
            
            cell.backgroundColor = UIColor.fromHexaString(hex: colorcodes().Port_Gore)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrackingHistoryCell", for: indexPath) as! TrackingHistoryCell
             cell.backgroundColor = UIColor.fromHexaString(hex: colorcodes().Port_Gore)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
             return 62
        }
        else if indexPath.section == 1
        {
            return 86
        }
        else
        {
            return 71
        }
       
    }
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
