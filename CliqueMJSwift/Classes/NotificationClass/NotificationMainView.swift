//
//  NotificationMainView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 13/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class NotificationMainView: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet weak var notificationMain_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib.init(nibName: "NotificationfirstCell", bundle: nil)
        self.notificationMain_table.register(nib, forCellReuseIdentifier: "NotificationfirstCell")

        
        self.notificationMain_table.rowHeight =  108
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationfirstCell", for: indexPath) as! NotificationfirstCell
        
        if indexPath.row == 0
        {
            cell.notification_back_img.image = #imageLiteral(resourceName: "notificationfirst")
        }
        else if indexPath.row == 1
        {
             cell.notification_back_img.image = #imageLiteral(resourceName: "notification2")
        }
        else
        {
             cell.notification_back_img.image = #imageLiteral(resourceName: "notification4")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            let receive = self.storyboard?.instantiateViewController(withIdentifier: "notifreceivedbudsView") as! notifreceivedbudsView
            self.present(receive, animated: true, completion: nil)
        }
        else if indexPath.row == 1
        {
            let receive = self.storyboard?.instantiateViewController(withIdentifier: "notificationLoyaityView") as! notificationLoyaityView
            self.present(receive, animated: true, completion: nil)
        }
        else if indexPath.row == 2
        {
            let receive = self.storyboard?.instantiateViewController(withIdentifier: "NotificationDeliveryView") as! NotificationDeliveryView
            self.present(receive, animated: true, completion: nil)
        }
        else if indexPath.row == 3
        {
            let show = self.storyboard?.instantiateViewController(withIdentifier: "NotificationreceivedBudsView") as! NotificationreceivedBudsView
            self.present(show, animated: true, completion: nil)
        }
        else if indexPath.row == 4
        {
//            let mainVc = self.storyboard?.instantiateViewController(withIdentifier: "DealReferFriendView") as! DealReferFriendView
//            let drawerVc = self.storyboard?.instantiateViewController(withIdentifier: "LeftDrawer") as! LeftDrawer
//
//            appDelegate.drawerController.mainViewController = mainVc
//            appDelegate.drawerController.drawerViewController = drawerVc
//            appDelegate.window?.rootViewController = appDelegate.drawerController
//            appDelegate.window?.makeKeyAndVisible()
            
            let show = self.storyboard?.instantiateViewController(withIdentifier: "DealReferFriendView") as! DealReferFriendView
            self.present(show, animated: true, completion: nil)
            
        }
    }

    @IBAction func mark_read_action(_ sender: Any)
    {
        let show = self.storyboard?.instantiateViewController(withIdentifier: "NotificationTrackView") as! NotificationTrackView
        self.present(show, animated: true, completion: nil)
    }
    @IBAction func dismiss_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
