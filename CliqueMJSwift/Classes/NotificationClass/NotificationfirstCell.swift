//
//  NotificationfirstCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 13/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class NotificationfirstCell: UITableViewCell {

    @IBOutlet weak var description_txt: UILabel!
    @IBOutlet weak var heading_txt: UILabel!
    @IBOutlet weak var notification_back_img: UIImageView!
    @IBOutlet weak var custom_view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
