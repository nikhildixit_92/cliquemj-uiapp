//
//  QRCodeProfileView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class QRCodeProfileView: UIViewController {

    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var qr_img: UIImageView!
    @IBOutlet weak var Ok_btn: UIButton!
    @IBOutlet weak var short_name_Txt: UILabel!
    @IBOutlet weak var user_name_txt: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.qr_img.image = #imageLiteral(resourceName: "sample_qr")
        
        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ok_btn_action(_ sender: Any)
    {
        
    }
    
}
