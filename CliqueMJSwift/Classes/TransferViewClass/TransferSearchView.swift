//
//  TransferSearchView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class TransferSearchView: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    @IBOutlet weak var recent_table: UITableView!
    @IBOutlet weak var search_txt: CustomUITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib.init(nibName: "TransferSearchCell", bundle: nil)
        self.recent_table.register(nib, forCellReuseIdentifier: "TransferSearchCell")
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransferSearchCell", for: indexPath) as! TransferSearchCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 68
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func scan_btn_action(_ sender: Any)
    {
        let reansfer = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeView") as! QRCodeView
        self.present(reansfer, animated: true, completion: nil)
    }
}
