//
//  ShareDispenseryView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class ShareDispenseryView: UIViewController {

    @IBOutlet weak var share_btn_img: UIImageView!
    @IBOutlet weak var share_link_img: UIImageView!
    @IBOutlet weak var share_whatts_img: UIImageView!
    @IBOutlet weak var share_message_img: UIImageView!
    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var share_btn: KButton!
    @IBOutlet weak var share_link_btn: KButton!
    @IBOutlet weak var share_whattsup_btn: KButton!
    @IBOutlet weak var share_Messager_btn: KButton!
    @IBOutlet weak var subheading_txt: UILabel!
    @IBOutlet weak var heading_txt: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.share_btn_img.layer.cornerRadius = 18.0
        self.share_btn_img.layer.masksToBounds = true
        
        self.share_link_img.layer.cornerRadius = 18.0
        self.share_link_img.layer.masksToBounds = true
        
        self.share_whatts_img.layer.cornerRadius = 18.0
        self.share_whatts_img.layer.masksToBounds = true
        
        self.share_message_img.layer.cornerRadius = 18.0
        self.share_message_img.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func not_now_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func share_btn_action(_ sender: Any)
    {
        
    }
    @IBAction func share_link_btn(_ sender: Any)
    {
        
    }
    @IBAction func share_whttsup_action(_ sender: Any)
    {
        
    }
    @IBAction func share_messaging_action(_ sender: Any)
    {
        let success  = self.storyboard?.instantiateViewController(withIdentifier: "pendingreferlView") as! pendingreferlView
        self.present(success, animated: true, completion: nil)
    }
}
