//
//  AgeView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 28/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class AgeView: UIViewController {

    @IBOutlet weak var age_img: UIImageView!
    @IBOutlet weak var custom_view: ANCustomView!
    @IBOutlet weak var confirm_btn: KButton!
    @IBOutlet weak var birth_date_txt: CustomUITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.custom_view.layer.cornerRadius = 5.0
        self.custom_view.layer.masksToBounds = true

        // Do any additional setup after loading the view.
    }

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirm_btn_action(_ sender: Any)
    {
        let onboarding = self.storyboard?.instantiateViewController(withIdentifier: "IntroView") as! IntroView
        self.present(onboarding, animated: true, completion: nil)
    }
}
