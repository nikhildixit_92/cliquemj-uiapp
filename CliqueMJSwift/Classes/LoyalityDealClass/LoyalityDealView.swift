//
//  LoyalityDealView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 14/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class LoyalityDealView: UIViewController {

    @IBOutlet weak var not_now_btn: UIButton!
    @IBOutlet weak var redeem_btn: UIButton!
    @IBOutlet weak var main_custom_view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.main_custom_view.clipsToBounds = true
        self.main_custom_view.layer.cornerRadius = 30
        self.main_custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.main_custom_view.isUserInteractionEnabled = true
        
        // Do any additional setup after loading the view.
    }

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func redeem_action(_ sender: Any)
    {
        let deal = self.storyboard?.instantiateViewController(withIdentifier: "LoyalityDealNextView") as! LoyalityDealNextView
        self.present(deal, animated: true, completion: nil)
    }
    
    @IBAction func not_now_action(_ sender: Any)
    {
        
    }
    
    @IBAction func share_btn_action(_ sender: Any)
    {
        
    }
}
