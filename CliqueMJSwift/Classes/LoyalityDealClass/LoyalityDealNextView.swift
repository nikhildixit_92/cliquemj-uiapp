//
//  LoyalityDealNextView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 14/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class LoyalityDealNextView: UIViewController {

    @IBOutlet weak var not_now_btn: UIButton!
    @IBOutlet weak var save_btn: UIButton!
    @IBOutlet weak var main_custom_view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.main_custom_view.clipsToBounds = true
        self.main_custom_view.layer.cornerRadius = 30
        self.main_custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.main_custom_view.isUserInteractionEnabled = true
        
        
        // Do any additional setup after loading the view.
    }

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func share_btn_Action(_ sender: Any)
    {
        
    }
    
    @IBAction func save_btn_action(_ sender: Any)
    {
        let Mjdeal = self.storyboard?.instantiateViewController(withIdentifier: "LoyltyDealSuccess") as! LoyltyDealSuccess
        self.present(Mjdeal, animated: true, completion: nil)
    }
    
    @IBAction func not_now_action(_ sender: Any)
    {
        
    }
}
