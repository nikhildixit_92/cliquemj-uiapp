//
//  EditProfileView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 30/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class EditProfileView: UIViewController {

    @IBOutlet weak var save_btn: KButton!
    @IBOutlet weak var change_password_btn: UIButton!
    @IBOutlet weak var email_txt: CustomUITextField!
    @IBOutlet weak var user_name_txt: CustomUITextField!
    @IBOutlet weak var name_txt: CustomUITextField!
    @IBOutlet weak var border_customView: UIView!
    @IBOutlet weak var user_pick_btn: UIButton!
    @IBOutlet weak var user_img: UIImageView!
    @IBOutlet weak var user_view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

       self.border_customView.clipsToBounds = true
        self.border_customView.layer.cornerRadius = 40
        self.border_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
//        self.user_view.layer.cornerRadius = self.user_view.frame.size.width/2
//        self.user_view.layer.masksToBounds = true
        
        self.user_img.layer.cornerRadius = self.user_img.frame.size.width/2
        self.user_img.layer.masksToBounds = true
        
        
        // Do any additional setup after loading the view.
    }

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func Change_password_action(_ sender: Any)
    {
        let change_pass  = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordView") as! ChangePasswordView
        self.present(change_pass, animated: true, completion: nil)
    }
    @IBAction func edit_intrest_btn(_ sender: Any)
    {
        let change_pass  = self.storyboard?.instantiateViewController(withIdentifier: "profileEditIntrestView") as! profileEditIntrestView
        self.present(change_pass, animated: true, completion: nil)
        
    }
    
    @IBAction func save_btn_action(_ sender: Any)
    {
        let redeem_view = self.storyboard?.instantiateViewController(withIdentifier: "RedeemVoucherView") as! RedeemVoucherView
        self.present(redeem_view, animated: true, completion: nil)
    }
    
    @IBAction func cancel_btn_action(_ sender: Any)
    {
        
    }
    
    @IBAction func Qrcode_scanner_action(_ sender: Any)
    {
        let qr_codeView = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeProfileView") as! QRCodeProfileView
        self.present(qr_codeView, animated: true, completion: nil)
    }
    
}
