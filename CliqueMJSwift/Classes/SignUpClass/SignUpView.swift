//
//  SignUpView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 06/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class SignUpView: UIViewController {

    @IBOutlet weak var phone_num_img: UIImageView!
    @IBOutlet weak var phone_number_txt: CustomUITextField!
    @IBOutlet weak var user_img_img: UIImageView!
    @IBOutlet weak var user_name_Txt: CustomUITextField!
    @IBOutlet weak var password_img: UIImageView!
    @IBOutlet weak var password_txt: CustomUITextField!
    @IBOutlet weak var confirm_img: UIImageView!
    @IBOutlet weak var confirm_txt: CustomUITextField!
    @IBOutlet weak var sign_in_btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.user_img_img.layer.cornerRadius = 5.0
        self.user_img_img.layer.masksToBounds = true
        
        self.password_img.layer.cornerRadius = 5.0
        self.password_img.layer.masksToBounds = true
        
        self.phone_num_img.layer.cornerRadius = 5.0
        self.phone_num_img.layer.masksToBounds = true
        
        self.confirm_img.layer.cornerRadius = 5.0
        self.confirm_img.layer.masksToBounds = true
        
        self.phone_num_img.layer.cornerRadius = 5.0
        self.phone_num_img.layer.masksToBounds = true

        // Do any additional setup after loading the view.
    }

    @IBAction func Sign_action(_ sender: Any)
    {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LoginView
        self.present(login, animated: true, completion: nil)
    }
    
    @IBAction func apple_id_sign(_ sender: Any)
    {
        
    }
    
    @IBAction func facebook_sign_action(_ sender: Any)
    {
        
    }
    
    @IBAction func show_password(_ sender: Any)
    {
        
    }
    @IBAction func confirm_password_show_Action(_ sender: Any)
    {
        
    }
    
    @IBAction func sign_up_action(_ sender: Any)
    {
        let Otp = self.storyboard?.instantiateViewController(withIdentifier: "OtpView") as! OtpView
        self.present(Otp, animated: true, completion: nil)
    }
}
