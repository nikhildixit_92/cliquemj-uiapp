//
//  ReferAFriendView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 30/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class ReferAFriendView: UIViewController {

    @IBOutlet weak var copy_code_btn: KButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func coy_code_action(_ sender: Any)
    {
        let reward_view = self.storyboard?.instantiateViewController(withIdentifier: "RewardsView") as! RewardsView
        self.present(reward_view, animated: true, completion: nil)
    }
    
    @IBAction func back_Action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
