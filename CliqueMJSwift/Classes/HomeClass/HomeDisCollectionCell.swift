//
//  HomeDisCollectionCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 11/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class HomeDisCollectionCell: UICollectionViewCell {

    @IBOutlet weak var deal_txt: UILabel!
    @IBOutlet weak var precent_txt: UILabel!
    @IBOutlet weak var img_img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
