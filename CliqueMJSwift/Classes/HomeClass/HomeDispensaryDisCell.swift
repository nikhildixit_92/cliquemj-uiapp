//
//  HomeDispensaryDisCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 11/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

protocol showpopup
{
    func showPop_up(dic:String);
}

class HomeDispensaryDisCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var show_delegate:showpopup?
    @IBOutlet weak var dispansery_img: UIImageView!
    @IBOutlet weak var dispensary_name_txt: UILabel!
    @IBOutlet weak var adress_txt: UILabel!
    @IBOutlet weak var star_txt: UILabel!
    @IBOutlet weak var star_icon: UIImageView!
    @IBOutlet weak var opclose_txt: UILabel!
    @IBOutlet weak var time_Txt: UILabel!
    @IBOutlet weak var loyality_txt: UILabel!
    @IBOutlet weak var collect_buds_btn: KButton!
    @IBOutlet weak var page_control_h: UIPageControl!
    @IBOutlet weak var hor_collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        let nib = UINib.init(nibName: "HomeDisCollectionCell", bundle: nil)
        self.hor_collectionView.register(nib, forCellWithReuseIdentifier: "HomeDisCollectionCell")
        
        
        // Initialization code
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeDisCollectionCell", for: indexPath) as! HomeDisCollectionCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize.init(width: self.hor_collectionView.frame.size.width, height: 120.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
             show_delegate?.showPop_up(dic: "0")
        }
        else
        {
            show_delegate?.showPop_up(dic: "1")
        }
        
       // show_delegate?.showPop_up(dic: NSDictionary())
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
