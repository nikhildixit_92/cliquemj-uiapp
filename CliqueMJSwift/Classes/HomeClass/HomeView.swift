//
//  HomeView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class HomeView: UIViewController,UITableViewDataSource,UITableViewDelegate,showpopup,Deal_click_check
{
    
    
   
    
    @IBOutlet weak var collect_buds_btn: UIButton!
    var dispensary_arr = [String]()
    @IBOutlet weak var home_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib.init(nibName: "HamburgerCell", bundle: nil)
        self.home_table.register(nib, forCellReuseIdentifier: "HamburgerCell")
        
        let nib1 = UINib.init(nibName: "HomeFirstCell", bundle: nil)
        self.home_table.register(nib1, forCellReuseIdentifier: "HomeFirstCell")
        
        let nib2 = UINib.init(nibName: "HomeSecondCell", bundle: nil)
        self.home_table.register(nib2, forCellReuseIdentifier: "HomeSecondCell")
        
        let nib3 = UINib.init(nibName: "HomeDispensaryTitleCell", bundle: nil)
        self.home_table.register(nib3, forCellReuseIdentifier: "HomeDispensaryTitleCell")
        
        let nib4 = UINib.init(nibName: "HomeDispensaryDisCell", bundle: nil)
        self.home_table.register(nib4, forCellReuseIdentifier: "HomeDispensaryDisCell")
        
        let nib5 = UINib.init(nibName: "HomeDispmoreCell", bundle: nil)
        self.home_table.register(nib5, forCellReuseIdentifier: "HomeDispmoreCell")
        
        let nib6 = UINib.init(nibName: "HomeBannerCell", bundle: nil)
        self.home_table.register(nib6, forCellReuseIdentifier: "HomeBannerCell")
        
        let nib7 = UINib.init(nibName: "CliqueDealsCell", bundle: nil)
        self.home_table.register(nib7, forCellReuseIdentifier: "CliqueDealsCell")
        
        
        let nib8 = UINib.init(nibName: "nearby_textCell", bundle: nil)
        self.home_table.register(nib8, forCellReuseIdentifier: "nearby_textCell")
        
        
        self.home_table.rowHeight = 122
        
        self.dispensary_arr = ["",""]
        
        
        let show_verify = self.storyboard?.instantiateViewController(withIdentifier: "VerifyPopUpView") as! VerifyPopUpView
        self.present(show_verify, animated: true, completion: nil)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        let drawer_id = UserDefaults.standard.value(forKey: "drawer_id") as? String ?? ""
        
        if drawer_id == "home"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
         }
        else if drawer_id == "editprofile"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileView") as! EditProfileView
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "Rewards"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "RewardsView") as! RewardsView
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "referfriend"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "ReferAFriendView") as! ReferAFriendView
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "myqrcode"
        {
             UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeProfileView") as! QRCodeProfileView
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "Changelocation"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "LocationView") as! LocationView
            self.present(dispensery, animated: true, completion: nil)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if dispensary_arr.count != 0
        {
             return 8
        }
        else
        {
             return 3
        }
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 3
        {
             return 6
        }
        else
        {
             return 1
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if dispensary_arr.count == 0
        {
            if indexPath.section == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HamburgerCell", for: indexPath) as! HamburgerCell
                cell.hamburger_btn.addTarget(self, action: #selector(hamburger_Action), for: .touchUpInside)
                cell.Bell_btn.addTarget(self, action: #selector(Bell_action), for: .touchUpInside)
                cell.search_btn.addTarget(self, action: #selector(Search_action), for: .touchUpInside)
                return cell
            }
            else if indexPath.section == 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFirstCell", for: indexPath) as! HomeFirstCell
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeSecondCell", for: indexPath) as! HomeSecondCell
                
                cell.Browse_btn.addTarget(self, action: #selector(show_browse_cat), for: .touchUpInside)
                
                return cell
                
            }
        }
        else
        {
            if indexPath.section == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HamburgerCell", for: indexPath) as! HamburgerCell
                cell.hamburger_btn.addTarget(self, action: #selector(hamburger_Action), for: .touchUpInside)
                cell.Bell_btn.addTarget(self, action: #selector(Bell_action), for: .touchUpInside)
                cell.search_btn.addTarget(self, action: #selector(Search_action), for: .touchUpInside)
                return cell
            }
            else if indexPath.section == 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFirstCell", for: indexPath) as! HomeFirstCell
                return cell
            }
            else if indexPath.section == 2
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDispensaryTitleCell", for: indexPath) as! HomeDispensaryTitleCell
                return cell
            }
            else if indexPath.section == 3
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDispensaryDisCell", for: indexPath) as! HomeDispensaryDisCell
                
                cell.show_delegate = self
                cell.collect_buds_btn.addTarget(self, action: #selector(show_collect_buds), for: .touchUpInside)
                return cell
                
            }
            else if indexPath.section == 4
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDispmoreCell", for: indexPath) as! HomeDispmoreCell
                
                cell.opticty_img.layer.cornerRadius = 19
                cell.opticty_img.layer.masksToBounds = true
                
                return cell
                
            }
            else if indexPath.section == 5
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeBannerCell", for: indexPath) as! HomeBannerCell
    
                return cell
                
            }
            else if indexPath.section == 6
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CliqueDealsCell", for: indexPath) as! CliqueDealsCell
                cell.Deal_clickedDelagate = self
                return cell
                
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "nearby_textCell", for: indexPath) as! nearby_textCell
                
                return cell
                
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if dispensary_arr.count == 0
        {
            if indexPath.section == 0
            {
                return 44
            }
            else if indexPath.section == 1
            {
                return 123
            }
            else
            {
                return 338
            }
        }
        else
        {
            if indexPath.section == 0
            {
                return 44
            }
            else if indexPath.section == 1
            {
                return 123
            }
            else if indexPath.section  == 2
            {
                 return 58
            }
            else if indexPath.section == 3
            {
                return UITableView.automaticDimension
            }
            else if indexPath.section == 4
            {
                return 122
            }
            else if indexPath.section == 7
            {
                return 44
            }
            else
            {
                return UITableView.automaticDimension
            }
           
        }
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 5
        {
            let refer = self.storyboard?.instantiateViewController(withIdentifier: "ReferAFriendView") as! ReferAFriendView
            self.present(refer, animated: true, completion: nil)
        }
        else if indexPath.section == 3
        {
            if self.dispensary_arr.count != 0
            {
                let dispensary_profile = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryDetailsView") as! DispenseryDetailsView
                
                self.present(dispensary_profile, animated: true, completion: nil)
            }
            else
            {
                
            }
            
           
        }
        else if indexPath.section == 1
        {
            let refer = self.storyboard?.instantiateViewController(withIdentifier: "LocationView") as! LocationView
            self.present(refer, animated: true, completion: nil)
        }
    }
    
    @objc func hamburger_Action()
    {
        if let drawerController = self.navigationController?.parent as? KYDrawerController
        {
            
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    
    @objc func collect_buds_action()
    {
        let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectBudsView") as! CollectBudsView
        self.present(collect, animated: true, completion: nil)
    }
    
    @objc func Bell_action()
    {
        let bell = self.storyboard?.instantiateViewController(withIdentifier: "NotificationMainView") as! NotificationMainView
        self.present(bell, animated: true, completion: nil)
    }
    
    @objc func Search_action()
    {
        let browse = self.storyboard?.instantiateViewController(withIdentifier: "BrowseView") as! BrowseView
        browse.come_from = "Browse"
        self.present(browse, animated: true, completion: nil)
    }
    
    @IBAction func collect_btn_action(_ sender: Any)
    {
        let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectLoyalityShortcutView") as! CollectLoyalityShortcutView
        self.present(collect, animated: true, completion: nil)
    }
    
    @objc func show_browse_cat()
    {
        let browse = self.storyboard?.instantiateViewController(withIdentifier: "BrowseView") as! BrowseView
        browse.come_from = ""
        self.present(browse, animated: true, completion: nil)
    }
    
    @objc func show_collect_buds()
    {
        let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectLoyalityShortcutView") as! CollectLoyalityShortcutView
        self.present(collect, animated: true, completion: nil)
    }
    
    func showPop_up(dic: String)
    {
        if dic == "0"
        {
            let loyality = self.storyboard?.instantiateViewController(withIdentifier: "LoyalityDealView") as! LoyalityDealView
            self.present(loyality, animated: true, completion: nil)
        }
        else
        {
            let loyality = self.storyboard?.instantiateViewController(withIdentifier: "MJDealView") as! MJDealView
            self.present(loyality, animated: true, completion: nil)
        }
    }
    
    //# MARK: Deal Clicked Delagate
    
    func DealClicked(Value: String)
    {
        let Cique_Deal = self.storyboard?.instantiateViewController(withIdentifier: "CliqueDealView") as! CliqueDealView
        self.present(Cique_Deal, animated: true, completion: nil)
    }
}
