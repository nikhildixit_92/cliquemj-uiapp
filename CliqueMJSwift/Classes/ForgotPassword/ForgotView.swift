//
//  ForgotView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 02/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class ForgotView: UIViewController {

    @IBOutlet weak var submit_btn: KButton!
    @IBOutlet weak var forgot_img: UIImageView!
    @IBOutlet weak var Email_address_txt: CustomUITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.forgot_img.layer.cornerRadius = 5.0
        self.forgot_img.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }

    @IBAction func submit_btn_Action(_ sender: Any)
    {
        let Welcome = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeFriendView") as! WelcomeFriendView
        self.present(Welcome, animated: true, completion: nil)
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
