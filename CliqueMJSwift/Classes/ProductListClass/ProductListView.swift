//
//  ProductListView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 03/06/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class ProductListView: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var search_collectionView: UICollectionView!
    @IBOutlet weak var header_collView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib.init(nibName: "productListCollCell", bundle: nil)
        self.header_collView.register(nib, forCellWithReuseIdentifier: "productListCollCell")
        
        let nib1 = UINib.init(nibName: "ProductListSecondCell", bundle: nil)
        self.header_collView.register(nib1, forCellWithReuseIdentifier: "ProductListSecondCell")
        
        
        let nib3 = UINib.init(nibName: "ProductSearchCollCell", bundle: nil)
        self.search_collectionView.register(nib3, forCellWithReuseIdentifier: "ProductSearchCollCell")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_dismiss_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        if collectionView == self.search_collectionView
        {
             return 1
        }
        else
        {
             return 2
        }
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == self.search_collectionView
        {
            return 8
        }
        else
        {
            if section == 0
            {
                return 1
            }
            else
            {
                return 5
            }
        }
        
       
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == self.search_collectionView
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductSearchCollCell", for: indexPath) as! ProductSearchCollCell
            
            return cell
        }
        else
        {
            if indexPath.section == 0
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productListCollCell", for: indexPath) as! productListCollCell
                
                return cell
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductListSecondCell", for: indexPath) as! ProductListSecondCell
                
                return cell
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        if collectionView == self.search_collectionView
        {
            
            
            return CGSize.init(width: collectionView.frame.size.width/2.0, height:  276.0)
        }
        else
        {
            return CGSize.init(width: 171.0, height: 63.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == self.search_collectionView
        {
            let show = self.storyboard?.instantiateViewController(withIdentifier: "ProductListDetailsView") as! ProductListDetailsView
            self.present(show, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
}
