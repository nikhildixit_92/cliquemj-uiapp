//
//  NotificationreceivedBudsView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 03/06/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class NotificationreceivedBudsView: UIViewController {

    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var dispensery_txt: UILabel!
    @IBOutlet weak var amount_txt: UILabel!
    @IBOutlet weak var from_name_txt: UILabel!
    @IBOutlet weak var dashboard_btn: KButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.custom_view.layer.cornerRadius = 5.0
        self.custom_view.layer.masksToBounds = true

        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dashboard_btn_action(_ sender: Any)
    {
        let loyality_noti = self.storyboard?.instantiateViewController(withIdentifier: "notifcationbudsrecBuds") as! notifcationbudsrecBuds
        self.present(loyality_noti, animated: true, completion: nil)
    }
    

}
