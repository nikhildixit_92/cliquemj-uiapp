//
//  DealReferFriendView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/06/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class DealReferFriendView: UIViewController
{
    
 @IBOutlet weak var copy_code_btn: KButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func hamburger_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func coy_code_action(_ sender: Any)
    {
        let reward_view = self.storyboard?.instantiateViewController(withIdentifier: "RewardsView") as! RewardsView
        self.present(reward_view, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool)
    {
        let drawer_id = UserDefaults.standard.value(forKey: "drawer_id") as? String ?? ""
        
        if drawer_id == "home"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let main = UIStoryboard.init(name: "Main", bundle: nil)
            let drawerC = main.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
            let navigation = UINavigationController.init(rootViewController: drawerC)
            navigation.setNavigationBarHidden(true, animated: true)
            appDelegate.window?.rootViewController = navigation
            appDelegate.window?.makeKeyAndVisible()
            self.present(navigation, animated: true, completion: nil)
            
        }
        else if drawer_id == "editprofile"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileView") as! EditProfileView
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "Rewards"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "RewardsView") as! RewardsView
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "referfriend"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "ReferAFriendView") as! ReferAFriendView
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "myqrcode"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeProfileView") as! QRCodeProfileView
            self.present(dispensery, animated: true, completion: nil)
        }
        else if drawer_id == "Changelocation"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            
            let dispensery = self.storyboard?.instantiateViewController(withIdentifier: "LocationView") as! LocationView
            self.present(dispensery, animated: true, completion: nil)
        }
    }

}
