//
//  profileEditIntrestView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 30/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class profileEditIntrestView: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var intrest_table_heignt: NSLayoutConstraint!
    @IBOutlet weak var intrest_table: UITableView!
    @IBOutlet weak var save_btn: KButton!
    @IBOutlet weak var border_customView: UIView!
    @IBOutlet weak var user_pick_btn: UIButton!
    @IBOutlet weak var user_img: UIImageView!
    @IBOutlet weak var user_view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.border_customView.clipsToBounds = true
        self.border_customView.layer.cornerRadius = 40
        self.border_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.user_img.layer.cornerRadius = self.user_img.frame.size.width/2
        self.user_img.layer.masksToBounds = true
        
        let nib = UINib.init(nibName: "productIntCell", bundle: nil)
        self.intrest_table.register(nib, forCellReuseIdentifier: "productIntCell")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true
            , completion: nil)
    }
    
    @IBAction func all_select_action(_ sender: Any)
    {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productIntCell", for: indexPath) as! productIntCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 65
    }
    
    override func viewWillLayoutSubviews()
    {
        self.intrest_table_heignt.constant = 5*65
    }
}
