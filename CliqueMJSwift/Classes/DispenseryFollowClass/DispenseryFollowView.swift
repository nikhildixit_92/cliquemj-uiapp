//
//  DispenseryFollowView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class DispenseryFollowView: UIViewController {

    @IBOutlet weak var dispensery_txt: UILabel!
    @IBOutlet weak var custom_vieew: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.custom_vieew.clipsToBounds = true
        self.custom_vieew.layer.cornerRadius = 40
        self.custom_vieew.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        // Do any additional setup after loading the view.
    }

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func following_btn(_ sender: Any)
    {
        let follow = self.storyboard?.instantiateViewController(withIdentifier: "ShareDispenseryView") as! ShareDispenseryView
        self.present(follow, animated: true, completion: nil)
    }
    
    @IBAction func switch_btn_action(_ sender: Any)
    {
        
    }
    @IBAction func follow_btn_Action(_ sender: Any)
    {
        
    }
}
