//
//  FollowRedeemView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 04/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class FollowRedeemView: UIViewController {

    var come_from = String()
    @IBOutlet weak var ok_btn: UIButton!
    @IBOutlet weak var dispensery_address: UILabel!
    @IBOutlet weak var dispensery_name_lbl: UILabel!
    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var qr_img: UIImageView!
    @IBOutlet weak var user_name_txt: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.qr_img.image = #imageLiteral(resourceName: "Sample_voc")
        
        self.custom_view.clipsToBounds = true
        self.custom_view.layer.cornerRadius = 40
        self.custom_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
    
        
        // Do any additional setup after loading the view.
    }

    @IBAction func ok_btn_Action(_ sender: Any)
    {
        let loy = self.storyboard?.instantiateViewController(withIdentifier: "successLoyality") as! successLoyality
        self.present(loy, animated: true, completion: nil)
    }
    @IBAction func not_now_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
