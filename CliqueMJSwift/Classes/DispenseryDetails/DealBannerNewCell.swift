//
//  DealBannerNewCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class DealBannerNewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var food_table: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let nib = UINib.init(nibName: "DealBannerNewCollCell", bundle: nil)
        self.food_table.register(nib, forCellWithReuseIdentifier: "DealBannerNewCollCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DealBannerNewCollCell", for: indexPath) as! DealBannerNewCollCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize.init(width: self.food_table.frame.size.width, height: self.food_table.frame.size.height)
    }
    
}
