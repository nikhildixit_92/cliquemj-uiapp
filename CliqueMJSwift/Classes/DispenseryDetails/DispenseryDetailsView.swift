//
//  DispenseryDetailsView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class DispenseryDetailsView: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var not_view_height: NSLayoutConstraint!
    var come_from = ""
    @IBOutlet weak var not_dis_btn: UIButton!
    @IBOutlet weak var not_view: UIView!
    
    @IBOutlet weak var dispensery_detaills_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib.init(nibName: "Dis_de_firstCell", bundle: nil)
        self.dispensery_detaills_table.register(nib, forCellReuseIdentifier: "Dis_de_firstCell")

        let nib1 = UINib.init(nibName: "dis_de_secondCell", bundle: nil)
        self.dispensery_detaills_table.register(nib1, forCellReuseIdentifier: "dis_de_secondCell")
    
        let nib2 = UINib.init(nibName: "dis_de_thirdCell", bundle: nil)
        self.dispensery_detaills_table.register(nib2, forCellReuseIdentifier: "dis_de_thirdCell")
        
        let nib3 = UINib.init(nibName: "dis_de_forthCell", bundle: nil)
        self.dispensery_detaills_table.register(nib3, forCellReuseIdentifier: "dis_de_forthCell")
        
        let nib4 = UINib.init(nibName: "DealsCell", bundle: nil)
        self.dispensery_detaills_table.register(nib4, forCellReuseIdentifier: "DealsCell")
        
        let nib5 = UINib.init(nibName: "DealCollectionView", bundle: nil)
        self.dispensery_detaills_table.register(nib5, forCellReuseIdentifier: "DealCollectionView")
        
        let nib6 = UINib.init(nibName: "DealBannerCell", bundle: nil)
        self.dispensery_detaills_table.register(nib6, forCellReuseIdentifier: "DealBannerCell")
        
        let nib7 = UINib.init(nibName: "DealBannerNewCell", bundle: nil)
        self.dispensery_detaills_table.register(nib7, forCellReuseIdentifier: "DealBannerNewCell")
    
        let nib8 = UINib.init(nibName: "DealProductCell", bundle: nil)
        self.dispensery_detaills_table.register(nib8, forCellReuseIdentifier: "DealProductCell")
        
        let nib9 = UINib.init(nibName: "RatingCell", bundle: nil)
        self.dispensery_detaills_table.register(nib9, forCellReuseIdentifier: "RatingCell")
        
        if come_from == "already"
        {
            self.not_view.isHidden = false
            self.not_view.clipsToBounds = true
            self.not_view_height.constant = 72.0
            self.not_view.layer.cornerRadius = 30
            self.not_view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
        else
        {
            self.not_view.isHidden = true
            self.not_view_height.constant = 0
           // self.dispensery_detaills_table.frame.size.height = self.view.frame.size.height
        }
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 14
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 4 || section == 6
        {
            return 5
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Dis_de_firstCell", for: indexPath) as! Dis_de_firstCell
            
            cell.back_btn.addTarget(self, action: #selector(dismiss_action), for: .touchUpInside)
            cell.follow_btn.addTarget(self, action: #selector(Follow_btn_Action), for: .touchUpInside)
            cell.collect_buds_btn.addTarget(self, action: #selector(collect_buds_action), for: .touchUpInside)
            cell.share_btn.addTarget(self, action: #selector(share_dispensary), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_secondCell", for: indexPath) as! dis_de_secondCell
            
            cell.website_view.layer.cornerRadius = cell.website_view.frame.size.width/2
             cell.Call_View.layer.cornerRadius = cell.Call_View.frame.size.width/2
             cell.orderView.layer.cornerRadius = cell.orderView.frame.size.width/2
             cell.exchangeView.layer.cornerRadius = cell.exchangeView.frame.size.width/2
            
            cell.exchange_btn.addTarget(self, action: #selector(TransferBudsView), for: .touchUpInside)
            cell.order_btn.addTarget(self, action: #selector(order_view), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_thirdCell", for: indexPath) as! dis_de_thirdCell
            
            return cell
            
        }
        else if indexPath.section == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_forthCell", for: indexPath) as! dis_de_forthCell
            
            cell.info_txt.text = "Loyalty Deals"
            
            return cell
        }
        else if indexPath.section == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealsCell", for: indexPath) as! DealsCell
            return cell
        }
        else if indexPath.section == 5
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_forthCell", for: indexPath) as! dis_de_forthCell
            
            cell.info_txt.text = "MJ Deals"
            
            return cell
        }
        else if indexPath.section == 6
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealsCell", for: indexPath) as! DealsCell
            return cell
        }
        else if indexPath.section == 7
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealCollectionView", for: indexPath) as! DealCollectionView
          
            return cell
            
        }
        else if indexPath.section == 8
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealBannerCell", for: indexPath) as! DealBannerCell
            
            return cell
            
        }
        else if indexPath.section == 9
        {
                let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_forthCell", for: indexPath) as! dis_de_forthCell
                
                cell.info_txt.text = "Nearby Munchies"
                
                return cell
        }
        else if indexPath.section == 10
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealBannerNewCell", for: indexPath) as! DealBannerNewCell
            
            return cell
            
        }
        else if indexPath.section == 11
        {
              let cell = tableView.dequeueReusableCell(withIdentifier: "dis_de_forthCell", for: indexPath) as! dis_de_forthCell
                
                cell.info_txt.text = "Popular Products"
                
                return cell
        }
        else if indexPath.section == 12
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealProductCell", for: indexPath) as! DealProductCell
            
            return cell
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingCell", for: indexPath) as! RatingCell
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
             return 449
        }
        else if indexPath.section == 1
        {
             return 137
        }
        else if indexPath.section == 2
        {
            return 263
        }
        else if indexPath.section == 3
        {
            return 82
        }
        else if indexPath.section == 4
        {
            return 141
        }
        else if indexPath.section == 5
        {
            return 82
        }
        else if indexPath.section == 6
        {
            return 141
        }
       else if indexPath.section == 7
        {
            return 346
        }
        else if indexPath.section == 8
        {
            return 135
        }
        else if indexPath.section == 9
        {
             return 82
        }
        else if indexPath.section == 10
        {
            return 179
        }
        else if indexPath.section == 11
        {
            return 82
        }
        else if indexPath.section == 12
        {
            return 279
        }
        else
        {
            return 95
        }
    }
    
    @IBAction func not_dis_action(_ sender: Any)
    {
        let follow_dis = self.storyboard?.instantiateViewController(withIdentifier: "FollowDispeneryLocationEnableView") as! FollowDispeneryLocationEnableView
        self.present(follow_dis, animated: true, completion: nil)
    }
    
    @objc func dismiss_action(sender:UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func collect_buds_action(sender:UIButton)
    {
        let collect = self.storyboard?.instantiateViewController(withIdentifier: "CollectLoyaltyStepOne") as! CollectLoyaltyStepOne
        self.present(collect, animated: true, completion: nil)
    }
    
    @objc func Follow_btn_Action()
    {
        let follow_screen = self.storyboard?.instantiateViewController(withIdentifier: "DispenseryFollowView") as! DispenseryFollowView
        self.present(follow_screen, animated: true, completion: nil)
    }
    
    @objc func TransferBudsView()
    {
        let tranfer_view = self.storyboard?.instantiateViewController(withIdentifier: "TransferBudsView") as! TransferBudsView
        self.present(tranfer_view, animated: true, completion: nil)
    }
    
    @objc func share_dispensary()
    {
        let share = self.storyboard?.instantiateViewController(withIdentifier: "ShareDispenseryView") as! ShareDispenseryView
        self.present(share, animated: true, completion: nil)
    }
    
    @objc func order_view(sender:UIButton)
    {
        let order = self.storyboard?.instantiateViewController(withIdentifier: "ProductListView") as! ProductListView
        self.present(order, animated: true, completion: nil)
    }
}
