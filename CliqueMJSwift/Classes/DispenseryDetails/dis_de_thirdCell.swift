//
//  dis_de_thirdCell.swift
//  CliqueMJSwift
//
//  Created by nikhil on 05/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import MapKit

class dis_de_thirdCell: UITableViewCell,MKMapViewDelegate {

    @IBOutlet weak var map_view: MKMapView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.map_view.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
