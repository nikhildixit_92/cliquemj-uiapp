//
//  BrowseView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 13/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit
import MapKit

class BrowseView: UIViewController,MKMapViewDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate
{
    
    var come_from = String()
    var select_value = 0
    @IBOutlet weak var table_originx: NSLayoutConstraint!
    @IBOutlet weak var search_table_view: UITableView!
    @IBOutlet weak var map_view: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib.init(nibName: "BrowseFirstCell", bundle: nil)
        self.search_table_view.register(nib, forCellReuseIdentifier: "BrowseFirstCell")
        
        let nib1 = UINib.init(nibName: "BrowseSecondCell", bundle: nil)
        self.search_table_view.register(nib1, forCellReuseIdentifier: "BrowseSecondCell")

         let nib2 = UINib.init(nibName: "NoDisCell", bundle: nil)
        self.search_table_view.register(nib2, forCellReuseIdentifier: "NoDisCell")
        
        
        self.search_table_view.rowHeight = 178
        self.map_view.delegate = self
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeUp.direction = .up
        swipeUp.delegate = self // set delegate
        self.search_table_view.addGestureRecognizer(swipeUp)
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            if come_from == "Browse"
            {
                return 5
            }
            else
            {
                return 1
            }
            
        }
        else
        {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseFirstCell", for: indexPath) as! BrowseFirstCell
            
            cell.main_customView.clipsToBounds = true
            cell.main_customView.layer.cornerRadius = 30
            cell.main_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            cell.main_customView.isUserInteractionEnabled = true
            
            let swipedown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
            swipedown.direction = .down
            swipedown.delegate = self // set delegate
            cell.main_customView.addGestureRecognizer(swipedown)
            
            if come_from == "Browse"
            {
                cell.following_btn.setTitle("FOLLOWING", for: .normal)
                cell.near_me_btn.setTitle("NEAR ME", for: .normal)
                
                if select_value == 0
                {
                    cell.opticity_img.isHidden = true
                    cell.opticity_img1.isHidden = false
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                }
                else
                {
                    cell.opticity_img.isHidden = false
                    cell.opticity_img1.isHidden = true
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                }
                
                cell.following_btn.addTarget(self, action: #selector(following_me_action), for: .touchUpInside)
                cell.near_me_btn.addTarget(self, action: #selector(nearby_me_Action), for: .touchUpInside)
            }
            else
            {
                cell.following_btn.setTitle("DISCOVER", for: .normal)
                cell.near_me_btn.setTitle("FOLLOWING", for: .normal)
                
                if select_value == 0
                {
                    cell.opticity_img.isHidden = true
                    cell.opticity_img1.isHidden = false
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                }
                else
                {
                    cell.opticity_img.isHidden = false
                    cell.opticity_img1.isHidden = true
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                }
                
                cell.following_btn.addTarget(self, action: #selector(Discover_action), for: .touchUpInside)
                cell.near_me_btn.addTarget(self, action: #selector(New_following_me), for: .touchUpInside)
            }
            
            return cell
        }
        else
        {
            if come_from == "Browse"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseSecondCell", for: indexPath) as! BrowseSecondCell
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoDisCell", for: indexPath) as! NoDisCell
                
                return cell
            }
            
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 99
        }
        else
        {
            if come_from == "Browse"
            {
                 return UITableView.automaticDimension
            }
            else
            {
                return 175
            }
           
        }
    }
    

    @IBAction func dismiss_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func search_btn_action(_ sender: Any)
    {
        
    }
    
    @IBAction func filter_btn_action(_ sender: Any)
    {
        let Drawer_list = self.storyboard?.instantiateViewController(withIdentifier: "DrawerListView") as! DrawerListView
        Drawer_list.come_from = "Browse"
        self.present(Drawer_list, animated: true, completion: nil)
    }
    
    @objc func nearby_me_Action()
    {
        self.select_value = 1
        self.search_table_view.reloadData()
    }
    
    @objc func following_me_action()
    {
        self.select_value = 0
        self.search_table_view.reloadData()
    }
    
    @objc func Discover_action()
    {
        self.select_value = 0
        self.search_table_view.reloadData()
    }
    
    @objc func New_following_me()
    {
        self.select_value = 1
        self.search_table_view.reloadData()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    // Debugging - All Swipes Are Detected Now
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")
                
                if self.table_originx.constant == 0.0
                {
                    self.table_originx.constant = (self.view.frame.size.height - 157.0) - 340.0
                }
                else
                {
                    
                }
                
            case  UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
                
                if self.table_originx.constant > 0.0
                {
                    self.table_originx.constant = 0.0
                }
                else
                {
                    
                }
                
            default:
                break
            }
        }
    }
}
