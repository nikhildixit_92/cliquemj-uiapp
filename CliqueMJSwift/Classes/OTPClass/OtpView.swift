//
//  OtpView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 06/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class OtpView: UIViewController {

    @IBOutlet weak var Resend_otp_btn: UIButton!
    @IBOutlet weak var submit_btn: UIButton!
    @IBOutlet weak var otp4_txt: CustomUITextField!
    @IBOutlet weak var otp1_txt: CustomUITextField!
    @IBOutlet weak var otp2_txt: CustomUITextField!
    @IBOutlet weak var opt3_txt: CustomUITextField!
    @IBOutlet weak var opt_img3: UIImageView!
    @IBOutlet weak var opt_img2: UIImageView!
    @IBOutlet weak var opt_img1: UIImageView!
    @IBOutlet weak var opt_img: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.opt_img.layer.cornerRadius = 5.0
        self.opt_img.layer.masksToBounds = true
        
        self.opt_img1.layer.cornerRadius = 5.0
        self.opt_img1.layer.masksToBounds = true
        
        self.opt_img2.layer.cornerRadius = 5.0
        self.opt_img2.layer.masksToBounds = true
        
        self.opt_img3.layer.cornerRadius = 5.0
        self.opt_img3.layer.masksToBounds = true


        // Do any additional setup after loading the view.
    }

    @IBAction func submit_btn_action(_ sender: Any)
    {
        let permission = self.storyboard?.instantiateViewController(withIdentifier: "AgeView") as! AgeView
        self.present(permission, animated: true, completion: nil)
    }
    
    @IBAction func resend_otp_action(_ sender: Any)
    {
        
    }
}
