//
//  DrawerListView.swift
//  CliqueMJSwift
//
//  Created by nikhil on 14/05/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import UIKit

class DrawerListView: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    var select_value = 0
    var come_from = String()
    @IBOutlet weak var mapSearch_table: UITableView!
    @IBOutlet weak var address_txt: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib.init(nibName: "DrawerListSearchCell", bundle: nil)
        self.mapSearch_table.register(nib, forCellReuseIdentifier: "DrawerListSearchCell")
        
        let nib1 = UINib.init(nibName: "DrawerListFirstCell", bundle: nil)
        self.mapSearch_table.register(nib1, forCellReuseIdentifier: "DrawerListFirstCell")

        let nib2 = UINib.init(nibName: "BrowseSecondCell", bundle: nil)
        self.mapSearch_table.register(nib2, forCellReuseIdentifier: "BrowseSecondCell")
        
        
         self.mapSearch_table.rowHeight = 178
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 2
        {
            return 5
        }
        else
        {
            return 1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerListSearchCell", for: indexPath) as! DrawerListSearchCell
            
            cell.main_customView.clipsToBounds = true
            cell.main_customView.layer.cornerRadius = 30
            cell.main_customView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            cell.main_customView.isUserInteractionEnabled = true
            cell.opticity_img.layer.cornerRadius = 31
            cell.opticity_img.layer.masksToBounds = true
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerListFirstCell", for: indexPath) as! DrawerListFirstCell
                cell.line_view.isHidden = true
            if come_from == "Browse"
            {
                cell.following_btn.setTitle("FOLLOWING", for: .normal)
                cell.near_me_btn.setTitle("NEAR ME", for: .normal)
                
                if select_value == 0
                {
                    cell.opticity_img.isHidden = true
                    cell.opticity_img1.isHidden = false
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                }
                else
                {
                    cell.opticity_img.isHidden = false
                    cell.opticity_img1.isHidden = true
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                }
                
                cell.following_btn.addTarget(self, action: #selector(following_me_action), for: .touchUpInside)
                cell.near_me_btn.addTarget(self, action: #selector(nearby_me_Action), for: .touchUpInside)
            }
            else
            {
                cell.following_btn.setTitle("DISCOVER", for: .normal)
                cell.near_me_btn.setTitle("FOLLOWING", for: .normal)
                
                if select_value == 0
                {
                    cell.opticity_img.isHidden = true
                    cell.opticity_img1.isHidden = false
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                }
                else
                {
                    cell.opticity_img.isHidden = false
                    cell.opticity_img1.isHidden = true
                    cell.following_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().text_color), for: .normal)
                    cell.near_me_btn.setTitleColor(UIColor.fromHexaString(hex: colorcodes().button_color), for: .normal)
                }
                
                cell.following_btn.addTarget(self, action: #selector(Discover_action), for: .touchUpInside)
                cell.near_me_btn.addTarget(self, action: #selector(New_following_me), for: .touchUpInside)
            }
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseSecondCell", for: indexPath) as! BrowseSecondCell
            
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 109
        }
        else if indexPath.section == 1
        {
            return 73
        }
        else
        {
            return UITableView.automaticDimension
        }
    }
    

    @IBAction func saerch_btn_action(_ sender: Any)
    {
        
    }
    
    @IBAction func filter_btn_action(_ sender: Any)
    {
        
    }
    
    @IBAction func dismiss_btn_Action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func nearby_me_Action()
    {
        self.select_value = 1
        self.mapSearch_table.reloadData()
    }
    
    @objc func following_me_action()
    {
        self.select_value = 0
        self.mapSearch_table.reloadData()
    }
    
    @objc func Discover_action()
    {
        self.select_value = 0
        self.mapSearch_table.reloadData()
    }
    
    @objc func New_following_me()
    {
        self.select_value = 1
        self.mapSearch_table.reloadData()
    }
}
