//
//  Constant.swift
//  CliqueMJSwift
//
//  Created by nikhil on 01/04/20.
//  Copyright © 2020 nikhil. All rights reserved.
//

import Foundation
import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate
//# MARK: color codes list

struct colorcodes
{
    let Steel_Grey = "1B1B29"
    let button_color = "2BC185"
    let Medium_Purple = "5B5BDE"
    let Sun_Glow = "2BC185"
    let Bittersweet = "FA7165"
    let Port_Gore = "202442"
    let Oxford_Blue = "363A55"
    let text_placeholder =  "9DB4D5"
    let text_color = "FFFFFF"

}

extension UILabel {
    
    func addImageWith(name: String, behindText: Bool) {
        
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: name)
        let attachmentString = NSAttributedString(attachment: attachment)
        
        guard let txt = self.text else {
            return
        }
        
        if behindText {
            let strLabelText = NSMutableAttributedString(string: txt)
            strLabelText.append(attachmentString)
            self.attributedText = strLabelText
        } else {
            let strLabelText = NSAttributedString(string: txt)
            let mutableAttachmentString = NSMutableAttributedString(attributedString: attachmentString)
            mutableAttachmentString.append(strLabelText)
            self.attributedText = mutableAttachmentString
        }
    }
}

extension UIColor
{
    class func fromHexaString(hex:String) -> UIColor
    {
        let scanner           = Scanner(string: hex)
        scanner.scanLocation  = 0
        var rgbValue: UInt64  = 0
        scanner.scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    
}


struct APIName
{
    let date_birth = "ws-set-date-of-birth"
    let registration = "ws-registration"
    let verify_otp = "ws-verify-top"
    let social_login = "ws-social-login"
    let login = "ws-login"
    let profile_data = "ws-user-profiledata"
    let resend_otp = "ws-otp-resend"
    let resend_expire = "ws-otp-expire"
    let forgot_pass = "ws-forgot-pass"
    let reset_password = "ws-reset-password"
    let get_user_dispensary = "ws-get-user-in-dispensary"
    let new_get_nearby_dispensary = "new-ws-get-nearby-dispensaries"
    let new_visit = "new-ws-visit"
    let interest_list = "new-ws-interest-list"
    let set_interest = "new-ws-set-interest"
    let get_interest = "new-ws-get-interest"
    let followd_dispensaries = "new-ws-get-followed-dispensaries"
    let follow_dispensary = "new-ws-follow-dispensary"
    let dispensary_details = "new-ws-get-dispensary-detail"
    let user_dispenseris = "new-ws-get-user-dispensaries"
    let productbydispensary = "new-ws-get-product-bydispenary"
    let product_detail = "new-ws-get-product-detail"
    let change_profile = "new-ws-change-profile"
    let check_nikname = "new-ws-check-nickname"
    let filter_dispensries = "new-ws-filter-dispensary"
    let sendbuds = "new-ws-send-budsms"
    let bud_by_dispensries = "new-ws-get-bud-by-dispensary"
    let search_user = "new-ws-search-user"
    let recent_user = "new-ws-recent-users"
    let recent_location = "new-ws-recent-locations"
    let transfer_buds = "new-ws-transfer-bud"
    let redeem = "new-ws-redeem"
    let generate_user_qr = "new-ws-generate-user-qr"
    let follow_collect = "new-ws-follow-collect"
    let get_my_coupan = "new-ws-get-my-coupons"
    let get_used_coupon = "new-ws-get-used-coupons"
    let reder_userdeal = "new-ws-refer-userdeal"
    let share_coupon = "new-ws-share-coupon"
    let get_bud_deal = "new-ws-get-bud-list"
    let scan_function = "new-ws-scan-function"
    let update_apn_device = "new-ws-update-apn-device"
    let update_fcm_device = "new-ws-update-fcm-device"
    let track_order = "new-ws-track-order"
    let new_track_order = "new-ws-track-order"
    let search_dispensry_name = "new-ws-search-dispensary-by-name"
    let newrewards = "new-ws-rewards"
    let signup_reward = "new-ws-signup-reward"
    let redeem_vocher = "new-ws-redeem-voucher"
    let new_share_dispensry = "new-ws-share-dispensary"
    let new_feedback = "new-ws-feedback"
    let privcy_policy = "new-ws-get-privacy-policy"
    let new_get_terms = "new-ws-get-terms"
    let get_buds_range = "new-ws-get-bud-range"
    let fliter_user_deal = "new-ws-filter-user-by-email"
    let coupon_expiry = "check-coupon-expiry"
    let customer_logout = "customer-logout"
    let offer_msg = "view-offer-msg-count"
    let get_offer = "ws-get-offer"
    let set_patient_data = "ws-set-patient-data"
    let delete_coupon = "ws-delete-coupon"
    let advert_offer = "ws-get-all-advertisement-offer"
    let redeem_offer = "ws-redeem-advertisement-offer"
    let ws_get_coupon = "ws-get-coupon"
    let get_patient_coupon = "ws-get-patient-coupon"
    let use_coupon = "ws-use-coupon"
    let coupon_details = "ws-get-coupon-details"
    let set_notification = "ws-set-notification"
}


struct APIKeys
{
    
}
